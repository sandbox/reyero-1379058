<?php 
/**
 * @file
 * Workbench translation pages
 */
/**
 * Returns a node submission form.
 */
function workbench_translation_page_node_add($type) {
  global $user;
  module_load_include('pages.inc', 'node');
  $types = node_type_get_types();
  $node = (object) array('uid' => $user->uid, 'name' => (isset($user->name) ? $user->name : ''), 'type' => $type, 'language' => LANGUAGE_NONE);
  drupal_set_title(t('Create @name', array('@name' => $types[$type]->name)), PASS_THROUGH);
  $output = drupal_get_form($type . '_node_form', $node);

  return $output;
}