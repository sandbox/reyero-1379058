<?php

/**
 * @file
 *  Content moderation views integration for Workbench.
 */

/**
 * Implements hook_views_data().
 */
function workbench_translation_views_data() {
  $data = array();
  
  $data['workbench_translation_node_history']['table']['group'] = t('Workbench Translation');
  $data['workbench_translation_node_history']['table']['join'] = array(
    'node' => array(
      'left_table' => 'node_revision',
      'left_field' => 'vid',
      'field' => 'vid',
      'type' => 'INNER',
    ),
    'node_revision' => array(
      'left_field' => 'vid',
      'field' => 'vid',
      'type' => 'INNER',
    ),
  );
  $data['workbench_translation_node_history']['hid'] = array(
    'title' => t('Translation History ID'),
    'help' => t('Content translation history record ID.'),
    'filter' => array(
      'handler' => 'views_handler_filter',
    ),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument',
    ),
  );
  $data['workbench_translation_node_history']['uid'] = array(
    'title' => t('User'),
    'help' => t('User who translated this revision.'),
    'relationship' => array(
      'title' => t('User'),
      'label' => t('moderator user'),
      'base' => 'users',
      'base field' => 'uid',
    ),
  );
  $data['workbench_translation_node_history']['source_vid'] = array(
    'title' => t('Source version'),
    'help' => t('Source version for this node translation.'),
    'filter' => array(
      'handler' => 'views_handler_filter',
    ),
    'field' => array(
      'handler' => 'views_handler_field_node_revision',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument',
    ),
    'relationship' => array(
      'title' => t('Source revision'),
      'label' => t('source revision'),
      'base' => 'node_revision',
      'base field' => 'vid',
    ),
  );
  $data['workbench_translation_node_history']['stamp'] = array(
    'title' => t('Timestamp'),
    'help' => t('The date this revision was translated.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  // Fields for the translation source nodes
  $data['workbench_moderation_node_history']['translation_actions'] = array(
    'title' => t('Translation Actions'),
    'help' => t('Content translation state of the node revisision.'),
    'real field' => 'state',
    'field' => array(
      'title' => t('Translation Links'),
      'handler' => 'workbench_translation_handler_field_links',
      'click sortable' => FALSE,
      'additional fields' => array('nid', 'vid', 'current'),
    ),
    'filter' => array(
      'title' => t('User Can Translate'),
      'handler' => 'workbench_translation_handler_filter_user_can_translate',
      'label' => t('Can translate'),
    ),
  );
  // Declare virtual fields on the {node} table.
  $data['node']['workbench_translation_moderated_type'] = array(
    'group' => t('Workbench Translation'),
    'title' => t('Type is translated'),
    'help' => t('Whether the content type is translated.'),
    'real field' => 'type',
    'filter' => array(
      'handler' => 'workbench_translation_handler_filter_moderated_type',
      'label' => t('Type is translated'),
      'type' => 'yes-no',
    ),
  );
  // Declare virtual fields on the {node} table.
  $data['node']['workbench_translation_node_is_translation'] = array(
    'group' => t('Workbench Translation'),
    'title' => t('Node is translation'),
    'help' => t('Whether the content is a translation.'),
    'real field' => 'tnid',
    'filter' => array(
      'handler' => 'workbench_translation_handler_filter_node_is_translation',
      'label' => t('Node is translation'),
      'type' => 'yes-no',
    ),
  );
  return $data;
}