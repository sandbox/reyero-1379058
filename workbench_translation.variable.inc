<?php
/**
 * @file
 * Definition of variables for workbench translation module.
 */

/**
 * Implements hook_variable_info().
 */
function workbench_translation_variable_info($options) {
  $variables['workbench_translation_publish_translations'] = array(
    'type' => 'boolean',
    'title' => t('Publish translations when source is published.', array(), $options),
    'default' => 0,
    'group' => 'workbench_translation',
  );
  $variables['workbench_translation_hold_translations'] = array(
    'type' => 'boolean',
    'title' => t('Prevent source from being published until all the translations are ready.', array(), $options),
    'default' => 0,
    'group' => 'workbench_translation',
  );
  return $variables;
}

/**
 * Implements hook_variable_group_info().
 */
function workbench_translation_variable_group_info() {
  $groups['workbench_translation'] = array(
    'title' => t('Workbench translation'),
    'description' => t('Workbench translation options'),
    'access' => 'administer workbench translation',
    'path' => array('admin/config/workbench/translation'),
  );
  return $groups;
}