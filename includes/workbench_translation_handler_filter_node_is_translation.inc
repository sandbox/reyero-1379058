<?php

/**
 * @file
 *  Provides moderation filters for Views.
 */

/**
 * Filter by whether a node type has moderation enabled or not.
 */
class workbench_translation_handler_filter_node_is_translation extends views_handler_filter_boolean_operator {
  function query() {
    if (!isset($this->value) || $this->value === NULL) {
      return;
    }
    $operator = $this->value ? '<>' : '=';
    $this->ensure_my_table();
    //$this->query->add_where($this->options['group'], "$this->table_alias.$this->real_field", "$this->table_alias.nid", $operator);
    $table = $this->table_alias;
    // Add condition tnid > 0 and tnid <> nid
    $this->query->add_where_expression($this->options['group'], "$table.$this->real_field > 0 AND $table.$this->real_field $operator $table.nid");
  }
}
